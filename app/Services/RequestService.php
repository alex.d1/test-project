<?php


namespace App\Services;


use App\Helpers\RequestServiceHelper;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Http\Request;

class RequestService
{
    protected $baseUrl = '';

    public function getData($url, $page = '')
    {
        $client = new Client();
        try {
            return $client->get($this->baseUrl . $url . '?token=' . RequestServiceHelper::getToken() . $page)
                ->getBody()
                ->getContents();
        } catch (GuzzleException $e) {
            return RequestServiceHelper::catchError($e, 'getData error: ', $url);
        }
    }

    public function sendPost($url, array $data)
    {
        $client = new Client();
        try {
            $data['token'] = RequestServiceHelper::getToken();

            return $client->post($this->baseUrl . $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                    ],
                    'body' => json_encode($data)
                ])->getBody()->getContents();
        } catch (GuzzleException $e) {
            return RequestServiceHelper::catchError($e, 'getData error: ', $url);
        }
    }

    public function sendPut($url, array $data)
    {
        $client = new Client();
        try {
            $data['token'] = RequestServiceHelper::getToken();

            return $client->put($this->baseUrl . $url,
                [
                    'headers' => [
                        'Content-Type' => 'application/json',
                    ],
                    'body' => json_encode($data)
                ])->getBody()->getContents();
        } catch (GuzzleException $e) {
            return RequestServiceHelper::catchError($e, 'getData error: ', $url);
        }
    }

    public function delete($url)
    {
        $client = new Client();
        try {
            return $client->delete($this->baseUrl . $url . '?token=' . RequestServiceHelper::getToken())->getBody()
                ->getContents();
        } catch (GuzzleException $e) {
            return RequestServiceHelper::catchError($e, 'getData error: ', $url);
        }
    }

    public function sendImage($url, Request $request)
    {
        $client = new Client();
        try {
            return $client->post($this->baseUrl . $url . '?token=' . RequestServiceHelper::getToken(),
                [
                    'multipart' => [
                        [
                            'name' => 'file',
                            'filename' => $request->file('file')->getClientOriginalName(),
                            'Mime-Type' => $request->file('file')->getmimeType(),
                            'contents' => fopen($request->file('file')->getPathname(), 'r'),
                        ],
                    ]
                ])->getBody()->getContents();
        } catch (GuzzleException $e) {
            return RequestServiceHelper::catchError($e, 'getData error: ', $url);
        }
    }
}
