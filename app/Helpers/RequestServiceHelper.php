<?php


namespace App\Helpers;


use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Log;

class RequestServiceHelper
{
    public static function catchError(Exception $e, $text, $url): JsonResponse
    {
        if ($e->getCode() === 403) {
            session(['token' => null]);

            return response()->json(['error' => 'forbidden'], 403);
        } else {
            Log::info($text . $url);
            Log::info('exception: ' . $e->getMessage());

            return response()->json(false, 400);
        }
    }

    public static function getToken()
    {
        $token = session('token', 123);
        if ($token === null) {
            throw new Exception('forbidden', 403);
        } else {
            return $token;
        }
    }
}
