<?php

namespace App\Http\Controllers\Email;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Email\ChangeReminderStatusRequest;
use App\Http\Requests\Email\ViewedProductsRequest;
use Illuminate\Http\Request;

class ViewedProductsController extends ApiController
{
    private $defaultUrl = 'email/viewed-products/';

    public function index()
    {
        return $this->service->getData($this->defaultUrl);
    }

    public function show($order)
    {
        return $this->service->getData($this->defaultUrl . $order);
    }

    public function update($order, ViewedProductsRequest $request)
    {
        return $this->service->sendPut($this->defaultUrl . $order, $request->all());
    }

    public function destroy($order)
    {
        return $this->service->delete($this->defaultUrl . $order);
    }

    public function store(Request $request)
    {
        return $this->service->sendPost($this->defaultUrl, $request->all());
    }

    public function updateStatus($order, ChangeReminderStatusRequest $request)
    {
        return $this->service->sendPut($this->defaultUrl . $order . '/status', $request->all());
    }

    public function getPicture($id)
    {
        return $this->service->getData($this->defaultUrl . $id . '/picture/');
    }

    public function storePicture($id, Request $request)
    {
        if ($request->hasFile("file")) {
            return $this->service->sendImage($this->defaultUrl . $id . '/picture/', $request);
        }

        return response()->json(false);
    }

    public function destroyPicture($id)
    {
        return $this->service->delete($this->defaultUrl . $id . '/picture/');
    }
}
