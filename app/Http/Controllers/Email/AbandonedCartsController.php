<?php

namespace App\Http\Controllers\Email;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Email\ChangeReminderStatusRequest;
use App\Http\Requests\Email\ReminderRequest;
use Illuminate\Http\Request;

class AbandonedCartsController extends ApiController
{
    private $defaultUrl = 'email/abandoned-carts/';

    public function index()
    {
        return $this->service->getData($this->defaultUrl);
    }

    public function show($id)
    {
        return $this->service->getData($this->defaultUrl . $id);
    }

    public function store()
    {
        return $this->service->sendPost($this->defaultUrl, []);
    }

    public function update($id, ReminderRequest $request)
    {
        return $this->service->sendPut($this->defaultUrl . $id, $request->all());
    }

    public function destroy($id)
    {
        return $this->service->delete($this->defaultUrl . $id);
    }

    public function updateStatus($id, ChangeReminderStatusRequest $request)
    {
        return $this->service->sendPut($this->defaultUrl . $id . '/status', $request->all());
    }

    public function sendPreview($id, Request $request)
    {
        return $this->service->sendPost($this->defaultUrl . 'reminder/preview/' . $id, $request->all());
    }

    public function getPicture($id)
    {
        return $this->service->getData($this->defaultUrl . $id . '/picture');
    }

    public function storePicture($id, Request $request)
    {
        if ($request->hasFile("file")) {
            return $this->service->sendImage($this->defaultUrl . $id . '/picture', $request);
        }

        return response()->json(false);
    }

    public function destroyPicture($id)
    {
        return $this->service->delete($this->defaultUrl . $id . '/picture/');
    }

}
