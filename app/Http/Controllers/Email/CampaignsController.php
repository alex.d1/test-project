<?php

namespace App\Http\Controllers\Email;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Email\CampaignRequest;
use Illuminate\Http\Request;

class CampaignsController extends ApiController
{
    private $defaultUrl = 'email/campaigns/';

    public function index()
    {
        return $this->service->getData($this->defaultUrl);
    }

    public function drafts(Request $request)
    {
        return $this->service->getData($this->defaultUrl . 'get/drafts', "&page=$request->page");
    }

    public function schedules(Request $request)
    {
        return $this->service->getData($this->defaultUrl . 'get/schedules', "&page=$request->page");
    }

    public function histories(Request $request)
    {
        return $this->service->getData($this->defaultUrl . 'get/histories', "&page=$request->page");
    }

    public function show($id)
    {
        return $this->service->getData($this->defaultUrl . $id);
    }

    public function store()
    {
        return $this->service->sendPost($this->defaultUrl, []);
    }

    public function update($id, CampaignRequest $request)
    {
        return $this->service->sendPut($this->defaultUrl . $id, $request->all());
    }

    public function destroy($id)
    {
        return $this->service->delete($this->defaultUrl . $id);
    }

    public function getPicture($id)
    {
        return $this->service->getData($this->defaultUrl . $id . '/picture/');
    }

    public function storePicture($id, Request $request)
    {
        if ($request->hasFile("file")) {
            return $this->service->sendImage($this->defaultUrl . $id . '/picture/', $request);
        }

        return response()->json(false);
    }

    public function destroyPicture($id)
    {
        return $this->service->delete($this->defaultUrl . $id . '/picture/');
    }
}
