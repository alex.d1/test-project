<?php

namespace App\Http\Controllers\SMS;

use App\Http\Controllers\ApiController;
use App\Http\Requests\Email\ChangeReminderStatusRequest;
use App\Http\Requests\SMS\CreateReminderRequest;
use Illuminate\Http\Request;

class AbandonedCartsController extends ApiController
{
    public function index()
    {
        return $this->service->getData('sms/abandoned-carts');
    }

    public function updateStatus($order, ChangeReminderStatusRequest $request)
    {
        return $this->service->sendPut('sms/abandoned-carts/' . $order . '/status', $request->all());
    }

    public function show()
    {
        return $this->service->getData('sms/abandoned-carts/reminders');
    }

    public function update(Request $request)
    {
        return $this->service->sendPut('sms/abandoned-carts', $request->all());
    }

    public function store(CreateReminderRequest $request)
    {
        return $this->service->sendPost('sms/abandoned-carts', $request->all());
    }

    public function destroy($order)
    {
        return $this->service->delete('sms/abandoned-carts/' . $order);
    }
}
