<?php

namespace App\Http\Controllers\SMS;

use App\Http\Controllers\ApiController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CountryController extends ApiController
{
    public function index()
    {
        return $this->service->getData('sms/countries');
    }

    public function store(Request $request)
    {
        return $this->service->sendPost('sms/countries', $request->all());
    }
}
