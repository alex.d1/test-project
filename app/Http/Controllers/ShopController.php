<?php

namespace App\Http\Controllers;

class ShopController extends ApiController
{
    public function index()
    {
        return $this->service->getData('shop');
    }
}
