<?php


namespace App\Http\Controllers;

use App\Services\WixRequestService;

class ApiController extends Controller
{
    public $service;

    public function __construct()
    {
        switch (session('service', 'wix')) {
            case 'wix':
                $this->service = new WixRequestService();
                break;
        }
    }
}
