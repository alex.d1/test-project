<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request): RedirectResponse
    {
        session(['token' => $request->token]);
        session(['service' => $request->platform]);

        return redirect()->route('dashboard', 'dashboard');
    }
}
