<?php

namespace App\Http\Controllers\Home;


use App\Http\Controllers\ApiController;
use App\Http\Requests\Email\ChangeReminderStatusRequest;
use Illuminate\Http\Request;

class EmailSubscriberController extends ApiController
{
    private $defaultUrl = 'home/email/subscribers/';

    public function getSubscribers(Request $request)
    {
        $page = $request->search ? "&page=$request->page&search=$request->search" : "&page=$request->page";

        return $this->service->getData($this->defaultUrl, $page);
    }

    public function deleteSubscriber($id)
    {
        return $this->service->delete($this->defaultUrl . $id);
    }

    public function changeStatus($id, ChangeReminderStatusRequest $request)
    {
        return $this->service->sendPut($this->defaultUrl . $id . '/status', $request->all());
    }

}
