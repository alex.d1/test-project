<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;

class FilterController extends ApiController
{
    public function filter(Request $request)
    {
        return $this->service->sendPost('home/filter', $request->all());
    }
}
