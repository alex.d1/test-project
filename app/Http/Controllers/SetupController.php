<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class SetupController extends ApiController
{
    private $defaultUrl = 'setup/finished';

    public function finished(Request $request)
    {
        return $this->service->sendPost($this->defaultUrl, $request->all());
    }
}
