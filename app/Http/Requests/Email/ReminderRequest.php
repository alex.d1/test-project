<?php

namespace App\Http\Requests\Email;

use Illuminate\Foundation\Http\FormRequest;

class ReminderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'send_time' => 'required|int',
            'send_time_type' => 'required|string|max:255',
            'subject' => 'required|string|max:255',
            'pre_header' => 'required|string|max:255',
            'title' => 'required|string|max:255',
            'paragraph_1' => 'required|string|max:255',
            'highlighted_text' => 'required|string|max:255',
            'button_text' => 'required|string|max:255',
            'paragraph_2' => 'string|max:255',
            'footer' => 'required|string|max:255',
            'code' => 'max:255',
            'automatic_discount_enabled' => 'boolean',
            'automatic_discount_value' => 'required|int',
            'colors' => 'required',
            'cart_offer_texts' => 'required|array|max:255',
            'fallback_text' => 'max:255',
            'status' => 'required'
        ];
    }
}
