<?php

namespace App\Http\Requests\Messenger;

use Illuminate\Foundation\Http\FormRequest;

class CustomerChatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'color' => 'required|string|min:3|max:255',
            'minimized' => 'required',
            'logged_in_greeting' => 'required|string|min:3|max:255',
            'logged_out_greeting' => 'required|string|min:3|max:255',
            'status' => 'boolean',
        ];
    }
}
