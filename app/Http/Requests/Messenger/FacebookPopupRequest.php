<?php

namespace App\Http\Requests\Messenger;

use Illuminate\Foundation\Http\FormRequest;

class FacebookPopupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:255',
            'subtitle' => 'required|string|min:3|max:255',
            'discount_text' => 'required|string|min:3|max:255',
            'discount_code' => 'required|string|min:3|max:255',
            'automatic_discount_enabled' => 'boolean',
            'automatic_discount_value' => 'int',
            'popup_type' => 'required|int',
            'when_display_value' => 'required|int',
            'when_display_type' => 'required|int',
            'icon_background' => 'required|string|min:3|max:255',
            'button_background' => 'required|string|min:3|max:255',
            'button_text_color' => 'required|string|min:3|max:255',
            'status' => 'boolean',
        ];
    }
}
