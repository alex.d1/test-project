<?php

namespace App\Http\Requests\Messenger;

use Illuminate\Foundation\Http\FormRequest;

class CartDiscountWidgetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|min:3|max:255',
            'subtitle' => 'required|string|min:3|max:255',
            'discount_text' => 'required|string|min:3|max:255',
            'discount_code' => 'required|string|min:3|max:255',
            'automatic_discount_enabled' => 'boolean',
            'automatic_discount_value' => 'int',
            'button_text_color' => 'required|string|min:3|max:255',
            'button_background' => 'required|string|min:3|max:255',
            'icon_background' => 'required|string|min:3|max:255',
            'coupon_button' => 'required|string|min:3|max:255',

        ];
    }
}
