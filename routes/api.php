<?php

use App\Http\Controllers\Email\AbandonedCartsController as EmailAbandonedCartsController;
use App\Http\Controllers\Email\CampaignsController as EmailCampaignsController;
use App\Http\Controllers\Email\ViewedProductsController as EmailViewedProductsController;
use App\Http\Controllers\Home\EmailSubscriberController;
use App\Http\Controllers\Home\FilterController;
use App\Http\Controllers\Home\SmsSubscriberController;
use App\Http\Controllers\SetupController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\SMS\AbandonedCartsController as SMSAbandonedCartsController;
use App\Http\Controllers\SMS\CountryController;
use Illuminate\Support\Facades\Route;

Route::get('shop', [ShopController::class, 'index']);
Route::prefix('setup')->group(function () {
    Route::post('finished', [SetupController::class, 'finished']);
});

Route::prefix('home')->group(function () {
    Route::post('filter', [FilterController::class, 'filter']);

    Route::get('email/subscribers', [EmailSubscriberController::class, 'getSubscribers']);
    Route::delete('email/subscribers/{emailSubscriber}', [EmailSubscriberController::class, 'deleteSubscriber']);
    Route::put('email/subscribers/{emailSubscriber}/status', [EmailSubscriberController::class, 'changeStatus']);

    Route::get('sms/subscribers', [SMSSubscriberController::class, 'getSubscribers']);
    Route::get('sms/subscribers/sent/{smsSubscriber}', [SMSSubscriberController::class, 'getSubscriberSent']);
    Route::delete('sms/subscribers/{smsSubscriber}', [SMSSubscriberController::class, 'deleteSubscriber']);
    Route::put('sms/subscribers/{smsSubscriber}/status', [SMSSubscriberController::class, 'changeStatus']);
});

Route::prefix('email')->group(function () {
    Route::prefix('abandoned-carts')->group(function () {
        Route::get('', [EmailAbandonedCartsController::class, 'index']);
        Route::get('/{id}', [EmailAbandonedCartsController::class, 'show']);
        Route::post('', [EmailAbandonedCartsController::class, 'store']);
        Route::put('/{id}', [EmailAbandonedCartsController::class, 'update']);
        Route::delete('/{id}', [EmailAbandonedCartsController::class, 'destroy']);
        Route::put('/{id}/status', [EmailAbandonedCartsController::class, 'updateStatus']);
        Route::post('/{id}/preview', [EmailAbandonedCartsController::class, 'sendPreview']);
        Route::get('/picture/{id}', [EmailAbandonedCartsController::class, 'getPicture']);
        Route::post('/picture/{id}', [EmailAbandonedCartsController::class, 'storePicture']);
        Route::delete('/picture/{id}', [EmailAbandonedCartsController::class, 'destroyPicture']);
    });

    Route::prefix('campaigns')->group(function () {
        Route::get('', [EmailCampaignsController::class, 'index']);
        Route::get('/drafts', [EmailCampaignsController::class, 'drafts']);
        Route::get('/schedules', [EmailCampaignsController::class, 'schedules']);
        Route::get('/histories', [EmailCampaignsController::class, 'histories']);
        Route::get('/{id}', [EmailCampaignsController::class, 'show']);
        Route::post('', [EmailCampaignsController::class, 'store']);
        Route::put('/{id}', [EmailCampaignsController::class, 'update']);
        Route::delete('/{id}', [EmailCampaignsController::class, 'destroy']);
        Route::get('/picture/{id}', [EmailCampaignsController::class, 'getPicture']);
        Route::post('/picture/{id}', [EmailCampaignsController::class, 'storePicture']);
        Route::delete('/picture/{id}', [EmailCampaignsController::class, 'destroyPicture']);
    });

    Route::prefix('browse-abandonment')->group(function () {
        Route::get('', [EmailViewedProductsController::class, 'index']);
        Route::get('/reminders/{id}',
            [EmailViewedProductsController::class, 'show']);
        Route::post('/reminders', [EmailViewedProductsController::class, 'store']);
        Route::put('/reminders/{id}',
            [EmailViewedProductsController::class, 'update']);
        Route::delete('/{id}',
            [EmailViewedProductsController::class, 'destroy']);
        Route::put('/reminders/{id}/status',
            [EmailViewedProductsController::class, 'updateStatus']);
        Route::get('/picture/{id}', [EmailViewedProductsController::class, 'getPicture']);
        Route::post('/picture/{id}', [EmailViewedProductsController::class, 'storePicture']);
        Route::delete('/picture/{id}', [EmailViewedProductsController::class, 'destroyPicture']);
    });
});

Route::prefix('sms')->group(function () {
    Route::prefix('abandoned-carts')->group(function () {
        Route::get('', [SMSAbandonedCartsController::class, 'index']);
        Route::post('', [SMSAbandonedCartsController::class, 'store']);
        Route::delete('/{order_id}', [SMSAbandonedCartsController::class, 'destroy']);
        Route::get('/reminders', [SMSAbandonedCartsController::class, 'show']);
        Route::put('/reminders', [SMSAbandonedCartsController::class, 'update']);
        Route::put('/reminders/{id}/status',
            [SMSAbandonedCartsController::class, 'updateStatus']);
    });

    Route::get('countries', [CountryController::class, 'index']);
    Route::post('countries', [CountryController::class, 'store']);
});
