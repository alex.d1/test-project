const mix = require('laravel-mix');
require('laravel-mix-alias');
require('laravel-mix-clean');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.alias({
    '@': '/resources/js',
    '~': '/resources/sass',
});

mix.webpackConfig({
    output: {
        chunkFilename: "[name].[chunkhash:8].js",
        filename: "[name].js",
    }
});

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css')
    .version()
    .clean({
        cleanOnceBeforeBuildPatterns: ['*.*.js'],
    });
