export default {
    methods: {
        getSegment(platform = $cookies.get('platform')) {
            switch(platform) {
                case 'shopify': return process.env.MIX_SHOPIFY_BACKEND_URL;                
                case 'wix': return process.env.MIX_WIX_BACKEND_URL;
            }
        }
    }
}

