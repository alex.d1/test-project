import axios from "axios"
import link from './backLink.js'
import authHeader from './authHeader.js'

const header = authHeader.methods

export async function ajaxGet(url, params, header = 'default') {
    try {
        let query = ''
        if(params && typeof params === 'object') {
            query += '?'
            for(const key in params) {
                query +=`${key}=${params[key]}`
            }
        }
        const { data } = await axios.get(getHost() + url + query, getHeader(header))
        return data  
    }  catch(err) {
        console.log(err)
        return false
    }
}

export async function ajaxDelete(url, id, header = 'default') {
    try {
        const  data  = await axios.delete(getHost() + url, getHeader(header), id)
        if(data.message || data.errors) return
        return data
    } catch(err) {
        console.log(err)
        return false
    }
}

export async function ajaxPost(url, payload, header = 'default') {
    try {
        const { data } = await axios.post(getHost() + url, payload, getHeader(header))
        return data
    }   catch(err) {
        console.log(err)
        return false
    }
}

export async function ajaxPut(url, payload, header = 'default') {
    try {
        const { data } = await axios.put(getHost() + url, payload, getHeader(header))
        return data
    } catch(err) {
        console.log(err)
        return false
    }
}

export function getHeader(type) {
    if(type === 'user') {
        return header.getUserHeader()
    }
    return header.getAuthHeader()
}

export function getHost() {
    return link.methods.getSegment()
}
