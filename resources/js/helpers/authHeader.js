export default {
    methods: {
        getAuthHeader(platform = $cookies.get('platform')) {
            return {
                headers: {Authorization: `Bearer ${$cookies.get(`token_${platform}`)}` }
            }
        },
        getUserHeader() {
            return {
                headers: {Authorization: `Bearer ${$cookies.get('userToken')}` }
            }
        },

    }
}
