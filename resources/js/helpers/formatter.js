import moment from "moment";

export default {
    install (Vue, options) {
        Vue.prototype.$formatter = {
            dateFormat: (dateString, format) => {
                return moment(dateString).format(format ? format : 'DD MMMM YYYY hh:mm');
            },
            dateFormatToISO: (dateString) => {
                return moment(dateString).toISOString();
            },
            stringReplace: (value, match, replacer) => {
                return value.replace(match, replacer)
            },
            fixedNumber: (item) => {
                if(item) {
                    let num = 0, string = ''
                    if(typeof item == 'string') {
                        num = parseFloat(item.match(/\d+/))
                        if(item.includes('.')) {
                            num = `${num}.${item.split('.')[1]}`
                            if(num[num.length - 1] == '.') num = num.substring(0, num.length - 2)
                            num = parseFloat(num)
                        }
                        string = item.split(num)[0]
                        num = num.toFixed(2)
                    } else num = String(item).includes('.') ? parseFloat(item).toFixed(2) : parseFloat(item)
                  
                    const isInt = num && String(num).indexOf('.') == -1 && num % 1 === 0;
                    if(isInt) return item
                    else if(item) return `${string}${num}`        
                    return 0.00
                }
                return item
            }
            // capitalizeText: (text) => {
            //     return text.charAt(0).toUpperCase() + text.slice(1);
            // },
            // latest_message_time: (dateString) => {
            //     if (moment.duration(moment().diff(moment(dateString))).asDays() >= 1) {
            //         return moment(dateString).format("MMMM DD");
            //     }
            //
            //     return moment(dateString).format("LT");
            // }
        }
    }
}
