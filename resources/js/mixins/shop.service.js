import headers from '../helpers/authHeader'
import link from '../helpers/backLink'
import router from "../router";
import {mapActions} from 'vuex'
const segmentShop = `https://wix.carti.io/api/shop`
const segmentSetup = `https://wix.carti.io/api/setup/finished`
const segmentDashboard = `https://wix.carti.io/api/home/filter`
const segmentBalance = `https://wix.carti.io/api/shop-get-balance`
export default {
    mixins: [headers, link],
    methods: {
        ...mapActions("shop", ["updShop"]),
        /**
         *
         * @returns {Promise<*>}
         */
        async getShop(load = false) {
            let shop
            if (load || !this.$session.exists("shop")) {
                const { data } = await axios.get(`${this.getSegment()}/shop`, this.getUserHeader())
                this.$session.set("shop", data);
                shop = data
            } else {
                shop = this.$session.get("shop")
            }

            this.updShop(shop)
           
            return shop
        },

        async skipTrial() {
            const requests = await axios.post(`${this.getSegment()}/shop/skip-trial `, {} ,this.getUserHeader())
            return requests.data
        },

        async getShopHeader() {
            const requests = await axios.get(`${this.getSegment()}/shop/header`, this.getUserHeader())
            return requests.data
        },

        async getBalance() {
            const request = await axios.get(`${this.getSegment()}/shop-get-balance`, this.getUserHeader())
            return request.data
        },

        async publishSetup(data) {
            return await axios.post(`${this.getSegment()}/setup/finished`, data, this.getUserHeader())
        },

        async getDashboardData(data) {
            return await axios.post (`${this.getSegment()}/home`, data, this.getUserHeader())
        },

        async segmentViewPricing() {
            return await axios.post(`${this.getSegment()}/segment/pricing-viewed`, {}, this.getUserHeader())
        },

        async segmentSkipped() {
            return await axios.post(`${this.getSegment()}/setup/skipped`, {}, this.getUserHeader())
        },

        async popupShopifyViewed() {
            return await axios.post(`${this.getSegment()}/segment/payment-popup-viewed`, {}, this.getUserHeader())
        },

        async popupShopifyExpanded() {
            return await axios.post(`${this.getSegment()}/segment/payment-popup-expanded`, {}, this.getUserHeader())
        },

        async createRecurringCharge() {
            const response = await axios.post(`${this.getSegment()}/create-recurring-charge`, {}, this.getUserHeader())
            return response.data
        }
    }
}
