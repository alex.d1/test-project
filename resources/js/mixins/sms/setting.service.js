import headers from '../../helpers/authHeader'
import link from '../../helpers/backLink'
export default {
    mixins: [headers, link],
    methods: {

        /**
         *
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllCountries() {
            return await axios.get(`${this.getSegment()}/sms/countries`, this.getUserHeader())
        },

    }
}
