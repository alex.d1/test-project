const segment = 'https://wix.carti.io/api/sms';
import link from '../../helpers/backLink'
import headers from '../../helpers/authHeader';

export default {
    mixins: [headers, link],
    methods: {

        /**
         *
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getSmsAbandonedCarts() {
            return await axios.get(`${this.getSegment()}/sms/abandoned-carts`, this.getUserHeader())
        },

        /**
         *
         * @param data
         * @returns {Promise<void>}
         */
        async createCart(data) {
            return await axios.post(`${this.getSegment()}/sms/abandoned-carts/`, data, this.getUserHeader()).then(response => {
                if (response && response.hasOwnProperty('data')) {
                    this.$router.push(`/sms/abandoned-carts/reminders/${response.data.order_id}`)
                }
            })
        },

        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async delete(id) {
            return await axios.delete(`${this.getSegment()}/sms/abandoned-carts/${id}`, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @param status
         * @returns {Promise<AxiosResponse<any>>}
         */
        async status(id, status) {
            return await axios.put(`${this.getSegment()}/sms/abandoned-carts/${id}/status`, {status}, this.getUserHeader())
        },

        /**
         *
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllReminders() {
            return await axios.get(`${this.getSegment()}/sms/abandoned-carts`, this.getUserHeader())
        },

        async save(data) {
            return await axios.put(`${this.getSegment()}/sms/abandoned-carts`, data, this.getUserHeader())
        },

        /**
         *
         * @param data
         * @returns {Promise<any>}
         */
        async getRegionAveragePriceByNumber(data) {
            const response = await axios.post(`${this.getSegment()}/sms/abandoned-carts/get-region-price`, data, this.getUserHeader())
            return response.data;
        },

        /**
         *
         * @param id
         * @param data
         * @returns {Promise<AxiosResponse<any>>}
         */
        async sendPreviewSms(id, data) {
            return await axios.post(`${this.getSegment()}/sms/abandoned-carts/${id}/preview`, data, this.getUserHeader())
        },
    }
}
