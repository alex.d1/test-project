import headers from '../../helpers/authHeader';
import link from '../../helpers/backLink';
import shop from '../../mixins/shop.service';

export default {
    mixins: [headers, link],
    methods: {
        /**
         *
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getCarts() {
            return await axios.get(`${this.getSegment()}/sms/campaigns`, this.getUserHeader())
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllDrafts(page, perPage) {
            return await axios.get(`${this.getSegment()}/sms/campaigns/drafts/?page=${page}&per_page=${perPage}`, this.getUserHeader())
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllCampaigns(page, perPage) {
            return await axios.get(`${this.getSegment()}/sms/campaigns/schedules/?page=${page}&per_page=${perPage}`, this.getUserHeader())
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllHistories(page, perPage) {
            return await axios.get(`${this.getSegment()}/sms/campaigns/histories/?page=${page}&per_page=${perPage}`, this.getUserHeader())
        },
        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getCampaignsById(id) {
            return await axios.get(`${this.getSegment()}/sms/campaigns/${id}`, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @param draft
         * @returns {Promise<AxiosResponse<any>>}
         */
        async update(id, draft) {
            return await axios.put(`${this.getSegment()}/sms/campaigns/${id}`, draft, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async delete(id) {
            return await axios.delete(`${this.getSegment()}/sms/campaigns/${id}`, this.getUserHeader())
        },

        async create(data) {
            return await axios.post(`${this.getSegment()}/sms/campaigns/`, data, this.getUserHeader())
        },

        async possiblePrice(count) {
            const response = await axios.get(`${this.getSegment()}/sms/campaigns/possible-price/${count}` , this.getUserHeader())
            return response.data
        },

        /**
         *
         * @param data
         * @returns {Promise<any>}
         */
        async getRegionAveragePriceByNumber(data) {
            const response = await axios.post(`${this.getSegment()}/sms/campaigns/get-region-price`, data, this.getUserHeader())
            return response.data;
        },

        /**
         *
         * @param id
         * @param data
         * @returns {Promise<AxiosResponse<any>>}
         */
        async sendPreviewSms(id, data) {
            return await axios.post(`${this.getSegment()}/sms/campaigns/${id}/preview`, data, this.getUserHeader())
        },

    }
}
