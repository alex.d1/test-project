import headers from '../../helpers/authHeader'
import link from '../../helpers/backLink'
const segment = 'https://wix.carti.io/api/sms';
export default {
    mixins: [headers, link],
    methods: {

        /**
         *
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllCountries() {
            return await axios.get(`${this.getSegment()}/sms/countries`, this.getUserHeader())
        },

        async updateCountries(data) {
            return await axios.post(`${this.getSegment()}/sms/countries`, data, this.getUserHeader())
        }
    }
}
