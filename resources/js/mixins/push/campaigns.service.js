import headers from '../../helpers/authHeader';
import link from '../../helpers/backLink';

export default {
    mixins: [headers, link],
    methods: {
        /**
         *
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getCarts() {
            return await axios.get(`${this.getSegment()}/push/campaigns`, this.getUserHeader())
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllDrafts(page) {
            return await axios.get(`${this.getSegment()}/push/campaigns/get/drafts/?page=${page}`, this.getUserHeader())
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllCampaigns(page) {
            return await axios.get(`${this.getSegment()}/push/campaigns/get/schedules/?page=${page}`, this.getUserHeader())
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllHistories(page) {
            return await axios.get(`${this.getSegment()}/push/campaigns/get/histories/?page=${page}`, this.getUserHeader())
        },
        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getCampaignsById(id) {
            return await axios.get(`${this.getSegment()}/push/campaigns/${id}`, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @param draft
         * @returns {Promise<AxiosResponse<any>>}
         */
        async save(id, draft) {
            if(draft.shop) delete draft.shop
            return await axios.put(`${this.getSegment()}/push/campaigns/${id}`, draft, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async delete(id) {
            return await axios.delete(`${this.getSegment()}/push/campaigns/${id}`, this.getUserHeader())
        },

        async create(data, firstOpen) {
            const response = await axios.post(`${this.getSegment()}/push/campaigns/`, data, this.getUserHeader())
            if (response && response.hasOwnProperty('data')) {
               await this.$router.push('/push/campaigns/add/' + response.data.id + "?first=" + firstOpen)
            }
            return response
        },

        async getPicture(id) {
            const response = await axios.get(`${this.getSegment()}/push/campaigns/${id}/picture`, this.getUserHeader())
            return response.data
        }
    }
}
