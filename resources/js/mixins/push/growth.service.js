import headers from '../../helpers/authHeader'
import link from '../../helpers/backLink';

export default {
    mixins: [headers, link],
    methods: {

        async getGrowthDashboard() {
            const response = await axios.get(`${this.getSegment()}/push/growth`, this.getUserHeader())
            return response.data
        },
        async getPopup() {
            const response = await axios.get(`${this.getSegment()}/push/popup`, this.getUserHeader())
            return response.data
        },
        async getWelcome() {
            const response = await axios.get(`${this.getSegment()}/push/welcome`, this.getUserHeader())
            return response.data
        },
        async updatePopup(data) {
            const response = await axios.put(`${this.getSegment()}/push/popup`, data, this.getUserHeader())
            return response.data
        },
        async updateWelcome(data) {
            if(data.shop) delete data.shop
            const response = await axios.put(`${this.getSegment()}/push/welcome`, data, this.getUserHeader())
            return response.data
        },
        async status(status) {
            const response = await axios.put(`${this.getSegment()}/push/status`, status, this.getUserHeader())
            return response.data
        },
    }
}
