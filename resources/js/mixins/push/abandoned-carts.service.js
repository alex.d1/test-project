const segment = 'https://wix.carti.io/api/push';
import headers from '../../helpers/authHeader';
import link from '../../helpers/backLink';

export default {
    mixins: [headers, link],
    methods: {
        async getPushAbandonedCarts() {
            const request = await axios.get(`${this.getSegment()}/push/abandoned-carts`, this.getUserHeader())
           return request.data
        },
        async createCart(data) {
            return await axios.post(`${this.getSegment()}/push/abandoned-carts/`, data, this.getUserHeader()).then(response => {
                if (response && response.hasOwnProperty('data')) {
                    this.$router.push(`/push/abandoned-carts/edit/${response.data.order}`)
                }
            })
        },
        async status(id, status) {
            return await axios.put(`${this.getSegment()}/push/abandoned-carts/${id}/status`, {status}, this.getUserHeader())
        },

        async save(data) {
            return await axios.put(`${this.getSegment()}/push/abandoned-carts`, data, this.getUserHeader())
        },

        async delete(id) {
            return await axios.delete(`${this.getSegment()}/push/abandoned-carts/${id}`, this.getUserHeader())
        },
    }
}
