import header from '../../helpers/authHeader'
import link from '../../helpers/backLink';
import backLink from "../../helpers/backLink";
const segment = 'https://wix.carti.io/api/admin'

export default {
    mixins: [header, link],
    methods: {
        async getShops(items) {
            let url = ''
            for(const key in items) {
                const value = items[key]
                if(value) url += `${!url.length ? '?' : '&'}${key}=${value}`
            }
            return await axios.get(`${this.getSegment()}/admin/shops${url}`, this.getAuthHeader())
        },
        async getShopById(id) {
            return await axios.get(`${this.getSegment()}/admin/shops/${id}`, this.getAuthHeader())
        },
        async update(id, shop) {
            return await axios.put(`${this.getSegment()}/admin/shops/${id}`, shop, this.getAuthHeader())
        },
        async login(id) {
            return await axios.get(`${this.getSegment()}/admin/shops/login/${id}`, this.getAuthHeader())
        },
        async delete(id) {
            return await axios.delete(`${this.getSegment()}/admin/shops/${id}`, this.getAuthHeader())
        },
        async getRefunds(id) {
            const response = await axios.get(`${this.getSegment()}/admin/shops/${id}/refunds`, this.getAuthHeader())
            return response.data
        },
        async createRefunds(id, data) {
            return await axios.post(`${this.getSegment()}/admin/shops/${id}/refunds`, data,  this.getAuthHeader())
        },
    }
}
