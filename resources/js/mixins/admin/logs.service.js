import headers from '../../helpers/authHeader';
import link from '../../helpers/backLink'

export default {
    mixins: [headers, link],
    methods: {

        async getLogs(page) {
            return await axios.get(`${this.getSegment()}/admin/logs?page=${page}`, this.getAuthHeader())
        },

        async clear() {
            return await axios.delete(`${this.getSegment()}/admin/logs`, this.getAuthHeader())
        },
        async test() {
            return await axios.put(`${this.getSegment()}/admin/logs`, {}, this.getAuthHeader())
        }
    }
}
