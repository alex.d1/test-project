import headers from '../../helpers/authHeader';
import link from '../../helpers/backLink'
const segment = 'https://wix.carti.io/api/admin/geo-permissions'

export default {
    mixins: [headers, link],
    methods: {
        async loadCountries() {
            const request = await axios.get(`${this.getSegment()}/admin/geo-permissions`, this.getAuthHeader())
            return request.data
        },
        async update(data) {
            return await axios.put(`${this.getSegment()}/admin/geo-permissions`, data,  this.getAuthHeader())
        }
    }
}
