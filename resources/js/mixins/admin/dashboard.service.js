import headers from '../../helpers/authHeader'
import link from "../../helpers/backLink";

const segment = 'https://wix.carti.io/api/admin'

export default {
    mixins: [headers, link],
    methods: {
        async getDashboard() {
            return await axios.get(`${this.getSegment()}/admin/dashboard`, this.getAuthHeader())
        },
    }
}
