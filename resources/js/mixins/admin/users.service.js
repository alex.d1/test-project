import header from '../../helpers/authHeader'
import link from '../../helpers/backLink';

export default {
    mixins: [header, link],
    methods: {
        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getUsers(page) {
            const response = await axios.get(`${this.getSegment()}/admin/users?page=${page}`, this.getAuthHeader())
            return response.data
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getWixUsers(page) {
            return await axios.get(`${this.getSegment('wix')}/admin/users?page=${page}`, this.getAuthHeader('wix')).then((res) => res.data.data).catch(() => [])
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getShopifyUsers(page) {
            return await axios.get(`${this.getSegment('shopify')}/admin/users?page=${page}`, this.getAuthHeader('shopify')).then((res) => res.data.data).catch(() => [])
        },

        /**
         *
         * @param id
         * @param platform
         * @returns {Promise<*>}
         */
        async getAdminUser(id, platform) {
            return await axios.get(`${this.getSegment(platform)}/admin/users/` + id, this.getAuthHeader(platform)).then((res) => res.data)
        },

        /**
         *
         * @param id
         * @param platform
         * @param data
         * @returns {Promise<*>}
         */
        async createAdminUser(id, platform, data) {
            return await axios.post(`${this.getSegment(platform)}/admin/users`, data, this.getAuthHeader(platform))
        },


        /**
         *
         * @param id
         * @param platform
         * @param data
         * @returns {Promise<*>}
         */
        async updateAdminUser(id, platform, data) {
            return await axios.put(`${this.getSegment(platform)}/admin/users/` + id, data, this.getAuthHeader(platform))
        },

        /**
         *
         * @param id
         * @param platform
         * @returns {Promise<*>}
         */
        async deleteAdminUser(id, platform) {
            return await axios.delete(`${this.getSegment(platform)}/admin/users/` + id, this.getAuthHeader(platform))
        }
    }
}
