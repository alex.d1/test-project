const segment = 'https://wix.carti.io/api/email';
import link from '../../helpers/backLink'
import headers from '../../helpers/authHeader'

export default {
    mixins: [headers, link],
    methods: {
        /**
         *
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAbandonedCarts() {
            return await axios.get(`${this.getSegment()}/email/abandoned-carts`, this.getUserHeader())
        },

        /**
         *
         * @returns {Promise<void>}
         */
        async createCart() {
            return await axios.post(`${this.getSegment()}/email/abandoned-carts`, {}, this.getUserHeader()).then(response => {
                if (response && response.hasOwnProperty('data')) {
                    this.$router.push('/email/abandoned-carts/reminders/' + response.data.id)
                }
            })
        },

        async sendPreviewEmail(id, email) {
            return await axios.post(`${this.getSegment()}/email/abandoned-carts/${id}/preview`, email, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async delete(id) {
            return await axios.delete(`${this.getSegment()}/email/abandoned-carts/${id}`, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @param status
         * @returns {Promise<AxiosResponse<any>>}
         */
        async status(id, status) {
            return await axios.put(`${this.getSegment()}/email/abandoned-carts/${id}/status`, {status}, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getById(id) {
            return await axios.get(`${this.getSegment()}/email/abandoned-carts/${id}`, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @param data
         * @returns {Promise<AxiosResponse<any>>}
         */
        async save(id, data) {
            return await axios.put(`${this.getSegment()}/email/abandoned-carts/${id}`, data, this.getUserHeader())
        }
    }
}
