const segment = 'https://wix.carti.io/api/email';
import link from '../../helpers/backLink'
import headers from '../../helpers/authHeader';
export default {
    mixins:[headers, link],
    methods: {

        /**
         *
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getCarts() {
            return await axios.get(`${this.getSegment()}/email/campaigns`, this.getUserHeader())
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllDrafts(page) {
            return await axios.get(`${this.getSegment()}/email/campaigns/get/drafts/?page=${page}`, this.getUserHeader())
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllCampaigns(page) {
            return await axios.get(`${this.getSegment()}/email/campaigns/get/schedules/?page=${page}`, this.getUserHeader())
        },

        /**
         *
         * @param page
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getAllHistories(page) {
            return await axios.get(`${this.getSegment()}/email/campaigns/get/histories/?page=${page}`, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getCampaignsById(id) {
            return await axios.get(`${this.getSegment()}/email/campaigns/${id}`, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @param draft
         * @returns {Promise<AxiosResponse<any>>}
         */
        async save(id, draft) {
            return await axios.put(`${this.getSegment()}/email/campaigns/${id}`, draft, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async delete(id) {
            return await axios.delete(`${this.getSegment()}/email/campaigns/${id}`, this.getUserHeader())
        },

        async create() {
            return await axios.post(`${this.getSegment()}/email/campaigns/`, {'test': 'test'}, this.getUserHeader()).then(response => {
                if (response && response.hasOwnProperty('data')) {
                    this.$router.push({name: 'email-campaigns-add', params: {id: response.data.id, first: true}})
                }
            })
        },

        /**
         *
         * @param id
         * @param data
         * @returns {Promise<AxiosResponse<any>>}
         */
        async sendPreviewEmail(id, data) {
            return await axios.post(`${this.getSegment()}/email/campaigns/${id}/preview`, data, this.getUserHeader())
        },
    }
}
