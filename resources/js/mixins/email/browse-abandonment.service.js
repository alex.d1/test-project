const segment = 'https://wix.carti.io/api/email/viewed-products';
import link from '../../helpers/backLink'
import headers from '../../helpers/authHeader';
export default {
    mixins: [headers, link],
    methods: {

        /**
         *
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getBrowseAbandonmentCarts() {
            return await axios.get(`${this.getSegment()}/email/viewed-products`, this.getUserHeader())
        },

        /**
         *
         * @returns {Promise<void>}
         */
        async createCart() {
            return await axios.post(`${this.getSegment()}/email/viewed-products`, {}, this.getUserHeader()).then(response => {
                if(response && response.hasOwnProperty('data')) {
                    this.$router.push('/email/browse-abandonment/reminders/' + response.data.id)
                }
            })
        },

        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async delete(id) {
            return await axios.delete(`${this.getSegment()}/email/viewed-products/${id}`, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @param status
         * @returns {Promise<AxiosResponse<any>>}
         */
        async status(id, status) {
            return await axios.put(`${this.getSegment()}/email/viewed-products/${id}/status`, {status}, this.getUserHeader())
        },

        /**
         *
         * @param id
         * @returns {Promise<AxiosResponse<any>>}
         */
        async getById(id) {
            return await axios.get(`${this.getSegment()}/email/viewed-products/${id}/`, this.getUserHeader())
        },

        async save(id, data) {
            return await axios.put(`${this.getSegment()}/email/viewed-products/${id}/`, data, this.getUserHeader())
        },
        /**
         *
         * @param id
         * @param data
         * @returns {Promise<AxiosResponse<any>>}
         */
        async sendPreviewEmail(id, data) {
            return await axios.post(`${this.getSegment()}/email/viewed-products/${id}/preview`, data, this.getUserHeader())
        },
    }
}
