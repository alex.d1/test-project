import headers from '../../helpers/authHeader'
import link from '../../helpers/backLink'
const segment = 'https://wix.carti.io/api'
export default {
    mixins: [headers, link],
    methods: {
       async loginUser(user) {
            return await axios.post(`${this.getSegment()+'/admin/login'}`, user, this.getUserHeader())
        }
    }
}
