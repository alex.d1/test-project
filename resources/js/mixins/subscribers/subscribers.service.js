import headers from '../../helpers/authHeader'
import link from '../../helpers/backLink'
const segment = 'https://wix.carti.io/api/home';

export default {
    mixins: [headers, link],
    methods: {
        async email(page, search) {
            const params = search  ? `&search=${search}` : ''
            return await axios.get(`${this.getSegment()}/home/email/subscribers/?page=${page}${params}`, this.getUserHeader())
        },
        async sms(page, search) {
            const params = search  ? `&search=${search}` : ''
            return await axios.get(`${this.getSegment()}/home/sms/subscribers/?page=${page}${params}`, this.getUserHeader())
        },
        async push(page, search) {
            // const params = search  ? `&search=${search}` : ''
            return await axios.get(`${this.getSegment()}/home/push/subscribers/?page=${page}`, this.getUserHeader())
        },
        // async search(page, tag, type, query) {
        //     return await axios.get(`${this.getSegment()}/home/sms/subscribers/?page=${page}/?tag=${tag}&tag_type=${type}&search=${query}`, this.getUserHeader())
        // },

        async delete(id) {
            return await axios.delete(`${this.getSegment()}/home/shops${id}`, this.getUserHeader())
        }
    }
}
