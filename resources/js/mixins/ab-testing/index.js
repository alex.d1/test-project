import {mapState} from 'vuex'

export default {
    data() {
        return {
            needConfirm: false,
            needDiscount: false,
        }
    },
    computed: {
       testingArray() {
            return this.$store.state.shop.abTesting?.array
       }
    },
    methods: {
        startABTesting() {
            if(!this.testingArray?.length) return
            this.testingArray.forEach(el => {
                const methodName = `get${el}`
                if(this[methodName]) {
                    this.$store.dispatch('shop/sendTestingResult', el)
                    this[methodName]()
                }
            })
        },
        getPricing_A() {
            return
        },
        getPricing_B() {
            return
            // this.plans[1].top = true
        },
        getPricing_C() {
            const firstPlan = this.plans[0]
            this.changePosition(0, this.plans.length - 1, firstPlan)
        },
        getPricing_D() {
            this.needConfirm = true
        },
        getPricing_E() {
            if(!this.shop.plan || this.shop.plan.toLowerCase() !== 'free') {
                this.needDiscount = true
                // const freeIndex = this.plans.findIndex(el => el.title.toLowerCase() == 'beginner')
                // const freeItem = this.plans[freeIndex] || this.plans[1]
                // this.plans.splice(freeIndex, 1)
                // this.plans = [freeItem, ...this.plans]
                // this.plans[0].top = true
            } 
        },
        getPricing_F() {
            this.$emit('showPricingF')
        },
        getEmailPlan_B() {
            this.showEmailPlan_B = true
        },
        changePosition(elementIndex, newIndex) {
            const element = this.plans[elementIndex]
            this.plans.splice(elementIndex, 1)
            this.plans.splice(newIndex, 1, element)
        },

    }
}
