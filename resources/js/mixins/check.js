export const check = {
    methods: {
        checkAvailability(platform, permissions) {
            return permissions.includes(platform)
        },
    }
} 