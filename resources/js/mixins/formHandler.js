export const formHandler = {
    methods: {
        getFormErrors(errors) {
                let string = ''
                for(const key in errors) {
                    if(errors[key].length) {
                        errors[key].forEach((el) => {
                            string += `${el},<br/>`
                        })
                    }
                        
                }
                this.$notify({
                    type: 'error',
                    title: 'Validation error',
                    text: string,
                });
        }
    }
} 