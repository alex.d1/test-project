import ReviewEventModal from '@/components/review-collector/Modal'
import api from '@/services/review-collector.service.js'

export const review = {
    data() {
        return {
            showReviewEventModal: false,
            modalReviewEventData: null,
        }
    },
    components: {
        ReviewEventModal
    },
    methods: {
        checkReviewEvent(data) {
            if(data) {
                this.modalReviewEventData = data
                this.showReviewEventModal = true
            }
        },
        closeReviewEventModal() {
            this.showReviewEventModal = false
            const {type} = this.modalReviewEventData
            if(type) api.modalClosed(type)           
        },
        async sendReview(review, load) {
            const {rating, text, source} = review 
            const data = await api.sendReview({rating, text, source})
            
            if(data) {
                this.$notify({
                    type: data.success ? 'success' : 'error',
                    title: data.message || 'Review sent'
                })
                if(data.redirect_url) window.open(data.redirect_url)
            }

            if(this.showReviewEventModal) {
                this.showReviewEventModal = false
                
                if(source == 'marketing_link') {
                    this.$router.push('/')
                    return
                }
            }

            if(load) this.filterData()
            this.closeReviewElement(data.status)
        },
        closeReviewElement() {
            if(this.$refs.review?.close) this.$refs.review.close()
        }
    }
}