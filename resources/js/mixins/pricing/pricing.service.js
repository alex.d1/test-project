import headers from '../../helpers/authHeader';
import link from '../../helpers/backLink'
const planSegment = `https://wix.carti.io/api/checkout-url`
const smsSegment = `https://wix.carti.io/api/checkout-sms-url`
export default {
    mixins: [headers, link],
    methods: {
        async loadPlans(title, param) {
            const request = await axios.get(`${this.getSegment()}/checkout-url?type=${title}&setup=${param}`, this.getUserHeader());
            return request.data
        },
        async smsCredits(type) {
            const request = await axios.get(`${this.getSegment()}/checkout-sms-url?type=${type}`, this.getUserHeader());
            return request.data
        }
    }
}
