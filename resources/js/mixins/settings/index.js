import api from '@/services/settings.service.js'
import shop from '@/mixins/shop.service'

export const settings = {
    mixins: [shop],
    methods: {
        getData() {
            for (const key in this.settings) {
                const item = this.settings[key]
                if (this.items.hasOwnProperty('value')) this.items[key].value = item.value
                else if (this.items.hasOwnProperty('from')) {
                    this.items[key].from = item.from
                    this.items[key].to = item.to
                } else this.items[key] = item

                if (this.items.hasOwnProperty('isActive')) this.items[key].isActive = value.isActive
            }
        },
        async checkVerify() {
            const data = await api.checkVerify()
            const {validate, cNames} = data
            this.isVerifyDomain.title = validate.valid
            this.loadingVerify = false
            if(cNames) {
                const domain = cNames.domain
                this.getCnameItems(domain, cNames)
                this.updInput(domain)
            }
            if(this.isVerifyDomain.title) this.updInput(this.$store.state.shop.shop.custom_domain || '')
            this.getValidationResult(validate)
        },  
        async verifyDomain(domain) {
            const {validate, cNames} = await api.checkVerify(domain)

            if(validate.valid) this.isVerifyDomain.alert.success = true
            else this.isVerifyDomain.alert.error = true

            this.getValidationResult(validate)

            if(this.isVerifyDomain.alert.success) {
                this.updStep()
                this.getShop(true)
            }
            this.$emit('clearLoads')
        },
        async getCnameItems(domain, items) {
            if(items) this.tableCnameItems = items
            else this.tableCnameItems = await api.getCnameItems(domain)
            this.updStep()
        },
        updStep() {
            this.step += 1
        },
        updInput(input) {
            this.$refs.enter_domain.updateValue(input)
        },
        getValidationResult(validate) {
            const validation_results = validate.validation_results
            this.isVerifyDomain.validation_results = []
            for(const key in validation_results) {
                this.isVerifyDomain.validation_results.push(validation_results[key].valid)
            }
        }
    }
}