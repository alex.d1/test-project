export const tags = {
    methods: {
        trackСhange(old, current, key, keynew) {
            const keys =
                typeof key !== 'object'
                    ? key.split('.')
                    : key.target.name.split('.')
            const keysLength = keys.length
            let oldValue, currentValue

            if (keysLength > 3) {
                oldValue = old[keys[0]][keys[1]]
                currentValue = current[keys[0]][keys[1]]
            } else {
                oldValue = old[keys[0]]
                currentValue = current[keys[0]]
            }

            const editedKey = keysLength > 1 ? keys[1] : keys[0]

            if (currentValue == '' && oldValue == null) oldValue = ''

            if (oldValue != currentValue)
                return {
                    value: true,
                    key: editedKey,
                }

            return {
                value: false,
                key: editedKey,
            }
        },
        createEditedObj(obj) {
            const value = {}
            for (const key in obj) {
                if (typeof obj[key] === 'object') {
                    for (const el in obj[key]) {
                        value[el] = false
                    }
                } else value[key] = false
            }
            return value
        },
        checkEditedItems(items) {
            for (const key in items) {
                if (typeof items[key] === 'object') {
                    for (const el in items[key]) {
                        if (value[el]) return true
                    }
                } else if (items[key]) return true
            }
            return false
        },
        addTag(textEditor, focusedName, focusedInput, selectedTagMenu) {
            if (!focusedName) return
            const keys = focusedName.split('.')
            let input = textEditor
            keys.forEach((el) => {
                input = input[el]
            })
            if (typeof input === 'string') {
                const len = input.length || 1
                const before = input.substr(0, focusedInput)
                const after = input.substr(focusedInput, len)
                const value = before + selectedTagMenu + after
                return {
                    keys,
                    value,
                }
            }
        },
    },
}
