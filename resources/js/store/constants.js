export const PLATFORMS = [
    {
        text: 'Wix',
        value: 'wix',
    },
    {
        text: 'Shopify',
        value: 'shopify',
    },
]
export const FULL_TIME = [
    '01:00',
    '02:00',
    '03:00',
    '04:00',
    '05:00',
    '06:00',
    '07:00',
    '08:00',
    '09:00',
    '10:00',
    '11:00',
    '12:00',
    '13:00',
    '14:00',
    '15:00',
    '16:00',
    '17:00',
    '18:00',
    '19:00',
    '20:00',
    '21:00',
    '22:00',
    '23:00',
]
export const QUIT_FULL_TIME = [
    '00:00',
    // '00:15',
    '00:30',
    // '00:45',
    '01:00',
    // '01:15',
    '01:30',
    // '01:45',
    '02:00',
    // '02:15',
    '02:30',
    // '02:45',
    '03:00',
    // '03:15',
    '03:30',
    // '03:45',
    '04:00',
    // '04:15',
    '04:30',
    // '04:45',
    '05:00',
    // '05:15',
    '05:30',
    // '05:45',
    '06:00',
    // '06:15',
    '06:30',
    // '06:45',
    '07:00',
    // '07:15',
    '07:30',
    // '07:45',
    '08:00',
    // '08:15',
    '08:30',
    // '08:45',
    '09:00',
    // '09:15',
    '09:30',
    // '09:45',
    '10:00',
    // '10:15',
    '10:30',
    // '10:45',
    '11:00',
    // '11:15',
    '11:30',
    // '11:45',
    '12:00',
    // '12:15',
    '12:30',
    // '12:45',
    '13:00',
    // '13:15',
    '13:30',
    // '13:45',
    '14:00',
    // '14:15',
    '14:30',
    // '14:45',
    '15:00',
    // '15:15',
    '15:30',
    // '15:45',
    '16:00',
    // '16:15',
    '16:30',
    // '16:45',
    '17:00',
    // '17:15',
    '17:30',
    // '17:45',
    '18:00',
    // '18:15',
    '18:30',
    // '18:45',
    '19:00',
    // '19:15',
    '19:30',
    // '19:45',
    '20:00',
    // '20:15',
    '20:30',
    // '20:45',
    '21:00',
    // '22:15',
    '21:30',
    '22:00',
    '22:30',
    // '22:45',
    '23:00',
    // '23:15',
    '23:30',
    // '23:45',
]
export const TYPE_TIME = [
    {text: 'Hours', value: 0},
    {text: 'Minutes', value: 1},
]
export const SHORT_TIME = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
    '24',
]

export const MINUTES = [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12',
    '13',
    '14',
    '15',
    '16',
    '17',
    '18',
    '19',
    '20',
    '21',
    '22',
    '23',
    '24',
    '25',
    '26',
    '27',
    '28',
    '29',
    '30',
    '31',
    '32',
    '33',
    '34',
    '35',
    '36',
    '37',
    '38',
    '39',
    '40',
    '41',
    '42',
    '43',
    '44',
    '45',
    '46',
    '47',
    '48',
    '49',
    '50',
    '51',
    '52',
    '53',
    '54',
    '55',
    '56',
    '57',
    '58',
    '59'
]
export const USER_ROLES = [
    {
        text: 'Select Role',
        disabled: true,
    },
    {
        value: '1',
        text: 'Admin',
    }, {
        value: '2',
        text: 'Support',
    },
];


export const emailTags = [
    {
        title: 'tags',
        disabled: true,
    },
    {
        title: '{domain}'
    },
    {
        title: '{firstName}'
    },
    {
        title: '{lastName}'
    },
    {
        title: '{siteName}'
    },
    {
        title: '{cartLink}Click here{/cartLink}'
    },
    {
        title: '{unsubscribeLink}Unsubscribe{/unsubscribeLink}'
    },
    {
        title: '{discountValue}'
    },
    {
        title: '{discountCode}'
    },
    {
        title: '{storeEmail}'
    },

]

export const emailCampaignsTags = [
    {
        title: 'tags',
        disabled: true,
    },
    {
        title: '{domain}'
    },
    {
        title: '{firstName}'
    },
    {
        title: '{lastName}'
    },
    {
        title: '{siteName}'
    },
    {
        title: '{campaignLink}Click here{/campaignLink}'
    },
    {
        title: '{unsubscribeLink}Unsubscribe{/unsubscribeLink}'
    },
    {
        title: '{discountValue}'
    },
    {
        title: '{discountCode}'
    },
    {
        title: '{storeEmail}'
    },

]

export const emailPreviewTags = [
    {
        title: 'tags',
        disabled: true,
    },
    {
        title: '{firstName}'
    },
    {
        title: '{lastName}'
    },
    {
        title: '{siteName}'
    },
    {
        title: '{discountValue}'
    },
    {
        title: '{discountCode}'
    },

]

export const pushTags = [
    {
        title: 'tags',
        disabled: true,
    },
    {
        title: '{firstName}'
    },
    {
        title: '{lastName}'
    },
    {
        title: '{siteName}'
    },
    {
        title: '{discountValue}'
    },
    {
        title: '{discountCode}'
    },
    {
        title: '{storeEmail}'
    },
]
 
export const plansWix = [
    {
        id: 1,
        title: 'Free',
        features: ['0$', 'up to 200 emails', '$0.5 free credit for SMS'],
        loading: false,
        top: false,
    },
    {
        id: 2,
        title: 'Beginner',
        features: ['4.99$', 'up to 1,000 emails', '$3 free credit for SMS'],
        loading: false,
        top: true,
    },
    {
        id: 3,
        title: 'Advanced',
        features: ['9.99$', 'up to 2,000 emails', '$5 free credit for SMS'],
        loading: false,
        top: false,
    },
    {
        id: 4,
        title: 'Expert',
        features: ['19.99$', 'up to 5,000 emails', '$8 free credit for SMS'],
        loading: false,
        top: false,
    },
    {
        id: 5,
        title: 'Pro',
        features: ['39.99$', 'up to 10,000 emails', '$10 free credit for SMS'],
        loading: false,
        top: false,
    },
    {
        id: 6,
        title: 'Plus',
        features: ['69.99$', 'up to 20,000 emails', '$15 free credit for SMS'],
        loading: false,
        top: false,
    },
    {
        id: 7,
        title: 'Rockstar',
        features: ['139.99$', 'up to 50,000 emails', '$20 free credit for SMS'],
        loading: false,
        top: false,
    },
    {
        id: 8,
        title: 'Superstar',
        features: ['199.99$', 'up to 100,000 emails', '$30 free credit for SMS'],
        loading: false,
        top: false,
    },
    {
        id: 9,
        title: 'Unlimited',
        features: ['349.99$', 'up to 200,000 emails', '$50 free credit for SMS'],
        loading: false,
        top: false,
    },
]