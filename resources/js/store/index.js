import Vue from "vue";
import Vuex from "vuex";
import { setField, setFieldObj, getError } from "@/helpers/store";

Vue.use(Vuex);
import shop from './modules/shop'
import review from './modules/review'

const vm = Vue.prototype

export default new Vuex.Store({
    state: {
        platform: 'Shopify',
    },
    mutations: {
        updPlatform(state, payload) {
            state.platform = payload
        },
        setField,
        setFieldObj,
        setNotif(state, payload) {
            vm.$notify({
                ...payload
            })
        },
    },
    actions: {

    },
    modules: {
        shop,
        review,
    }
  })