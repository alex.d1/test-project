import { setField, setFieldObj, getError } from "../../helpers/store";
import {getHeader, getHost} from '@/helpers/ajax'

export default {
    namespaced: true,
    state: {
      shop: null,
      discountLink: null,
      currency: null,
    },
    mutations: {
      setField,
      setFieldObj,
    },
    actions: {
      updShop({state, commit, dispatch}, value) {
        commit('setField', {
          field: 'shop', value
        })

        dispatch('updDiscountLink', value)
        dispatch('updABTesting',  value.ab_variants)
        dispatch('updCurrency', value.payment_currency)
      },
      updDiscountLink({commit}, value) {
        const isShopify = window.$cookies.get('platform') === 'shopify'
        let link = `${value.site_url}/admin/discounts`

        if(link.includes('//a')) {
          link = link.slice('//a').split('/')
        }

        const discountLink = isShopify ? link : null
        commit('setField', {
          field: 'discountLink', value: discountLink,
        })
      },
      updABTesting({commit}, ab_variants) {
        if(ab_variants) {
          const abNameArray = Object.values(ab_variants)
          const abIdArray = Object.keys(ab_variants)

          commit('setField', {
            field: 'abTesting', value: {array: abNameArray, ids: abIdArray},
          })
        }
      },
      updCurrency({commit}, payment_currency) {
        let currency = ''
        switch(payment_currency) {
          case 'USD': {
            currency = '$'
            break
          }
          case 'EUR': {
            currency = '€'
            break
          }
          case 'UAH': {
            currency = '₴'
            break
          }
          case 'ILS': {
            currency = '₪'
            break
          }
        }

        commit('setField', {
          field: 'currency', value: currency,
        })
      },
      sendTestingResult({state}, payload = 'Pricing_A') {
        const host = getHost().split('/api')[0]
        const indexExperiment = state.abTesting.array.indexOf(payload)
        const id = state.abTesting.ids[indexExperiment]

        axios.post(`${host}/ab-testing/${state.shop.shop_id}/${id}`, {}, getHeader('user'))
      }
    },
}