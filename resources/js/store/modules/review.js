import { setField, setFieldObj, getError } from "../../helpers/store";
import {getHeader, getHost} from '@/helpers/ajax'

export default {
    namespaced: true,
    state: {
      showAddReview: false,
    },
    mutations: {
      setField,
      setFieldObj,
    },
}