window.$ = window.jQuery = require('jquery');
require('./bootstrap');
require('./interceptors');
window.Vue = require('vue');
window.moment = require('moment');
window.events = new Vue();

import VueRouter from "vue-router";
import App from './components/App';
import router from "./router";
import {formHandler} from  '@/mixins/formHandler'
import {BootstrapVue, IconsPlugin} from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import ToggleButton from 'vue-js-toggle-button';
import '../css/global.scss';
import '../css/colors.scss';
import '../css/pricing.scss';
import formatter from './helpers/formatter'
import VueCookies from 'vue-cookies'
import VueSessionStorage from "vue-sessionstorage";
import DraftsTable from './components/sms/campaigns/DraftsTable.vue'
import CampaignsTable from './components/sms/campaigns/CampaignsTable.vue'
import HistoriesTable from './components/sms/campaigns/HistoriesTable.vue'
import store from './store/index'
import {
    ValidationObserver,
    ValidationProvider,
    extend,
    localize,
} from "vee-validate";
import en from "vee-validate/dist/locale/en.json";
import * as rules from "vee-validate/dist/rules";
import timezoneMoment from 'moment-timezone';
import moment from 'moment';
import VueMoment from 'vue-moment';
import Paginate from 'vuejs-paginate';
import Notifications from "vue-notification";
import Buttons from '@/components/elements/Buttons.vue'
import Vue from "vue";
import {SimpleSVG} from 'vue-simple-svg'

localize("en", en);

Vue.prototype.$notify = Notifications
Vue.prototype.$timezoneMoment = timezoneMoment
Vue.prototype.$moment = moment

Vue.component('Paginate', Paginate)
Vue.component("ValidationObserver", ValidationObserver);
Vue.component("ValidationProvider", ValidationProvider);
Vue.component("DraftsTable", DraftsTable);
Vue.component("CampaignsTable", CampaignsTable);
Vue.component("Buttons", Buttons);
Vue.component("HistoriesTable", HistoriesTable);
Vue.component('Pagination', require('laravel-vue-pagination'));
Vue.component('SimpleSvg', SimpleSVG)

Vue.use(formatter);
Vue.use(VueMoment, {moment,});
Vue.use(Notifications);
Vue.use(VueCookies);
Vue.use(VueSessionStorage);
Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(ToggleButton);
Vue.mixin(formHandler);


Object.keys(rules).forEach(rule => {
    extend(rule, rules[rule]);
});

const app = new Vue({
    el: '#app',
    store,
    render: h => h(App),
    router,
});
