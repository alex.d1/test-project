import { ajaxGet, ajaxDelete, ajaxPost, ajaxPut } from '../helpers/ajax'

export default {
    async getSetting(){
        const {data} = await ajaxPost('/setting/index-data', '', 'user');
        return data
     },

    async updateSetting(payload){
        return ajaxPut('/setting/index-data', payload, 'user');
    },

    async updateCountries(payload) {
        return ajaxPost('/sms/countries', payload, 'user');
    },

    async checkVerify(domain) {
        let payload = domain ? {domain} : {}
        return ajaxPost('/setting/validate-domain', payload, 'user')
    },

    async getCnameItems(domain) {
        return ajaxPost('/setting/custom-domain', {domain}, 'user')
    },

    async deleteCnameItems() {
        return ajaxPost('/setting/delete-domain', {}, 'user')
    },
    async getAllCountries() {
        return ajaxGet('/sms/countries', {}, 'user')
    }
}
