
import { ajaxPost, ajaxPut } from '@/helpers/ajax'

export default {
    async sendReview(review) {
        return await ajaxPost('/shop/review', review, 'user')  
    },
    async modalClosed(type) {
        ajaxPut(`/shop/review/event/${type}`, {}, 'user')
    }
}