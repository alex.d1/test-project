
import { ajaxPost } from '../helpers/ajax'

export default {
    async login(user, platform) {
        window.$cookies.remove('platform')
        window.$cookies.set('platform', platform)

        const header = 'user'
        const { token } = await ajaxPost('/admin/login', user, header)
        if(token) {
            window.$cookies.set(`token_${platform}` , token)
            return true
        }
    }
}