
import { ajaxPost } from '../helpers/ajax'

export default {
    async imageUpload(type, id, payload) {
        await ajaxPost(`/growth/${id}/update-image-${type}`, payload, 'user')
    }
}
