import { ajaxGet, ajaxDelete, ajaxPost, ajaxPut } from '../helpers/ajax'

export default {
    async getItems() {
        const { data } = await ajaxGet('/admin/discount-links')
        return data
    },
    async deleteItem(id) {
        return await ajaxDelete(`/admin/discount-links/${id}`)
    },
    async createItem(payload) {
        return  await ajaxPost('/admin/discount-links/', payload)
    },
    async updateItem(payload) {
        return await ajaxPut(`/admin/discount-links/${payload.id}`, payload)
    }
}
