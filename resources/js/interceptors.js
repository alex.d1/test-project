import axios from 'axios';
import router from "./router";
import Vue from 'vue'
const vm = Vue.prototype

axios.interceptors.response.use(response => {
        if (response) {
            checkCookiesData()
            return Promise.resolve(response)
        }
        else return Promise.reject(response)
        
    },
    error => {
        const status = error.response.status
        
        if (status) {
            const method = error.config.method
            const url = error.config.url

            switch (status) {
                case 429: 
                    vm.$notify({
                        type: 'error',
                        title: 'Too many requests'
                    })
                    break
                case 401:
                case 403:
                    router.push({
                        name: 'admin',
                    }).catch(err => {})
                    break
                default:
                    checkCookiesData()
                    const data = getErrors(error.response.data, method, url)
                    vm.$notify({
                        type: 'error',
                        title: data.title,
                        text: data.text,
                    })
            }
            return Promise.reject(error.response)
        }
    }
)
function getErrors(data, method, url) {
    let text = ''
    let title = 'Error. Please try again later'
    if(typeof data === 'string') {
        title = data
    } else {
        if(data.message) title = `${data.message}`

        const errors = data.errors
        if(errors) {
            text = ''
            for(const key in errors) {
                if(Array.isArray(errors[key])) {
                    errors[key].forEach(el => {
                        text += `—${errors[key]}\n`
                    })
                }
            }
        }
    }

    return {title, text}
}

function checkCookiesData() {
    if(!vm.$cookies.get('platform')) {
        vm.$cookies.remove('userToken')
        router.push({
        name: 'admin',
        }).catch(err => {})
        return
    }

    if(!window.location.pathname.includes('admin') && !vm.$cookies.get('userToken')) {
        router.push({
        name: 'admin-dashboard',
        }).catch(err => {})
        return
    }
}