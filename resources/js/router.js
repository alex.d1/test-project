import VueRouter from 'vue-router';

const router = new VueRouter({
    linkActiveClass: "active",
    routes: [
        {
          path: '/',
          redirect: '/dashboard',
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/home/Dashboard'),
            beforeEnter: (to, from, next) => {                
                const platform = to.query.platform
                const token = to.query.token

                if(!token && !platform) {
                    const keys = window.$cookies.get('token_wix') && window.$cookies.get('token_shopify') 
                    const userToken = window.$cookies.get('userToken')
                    if(!userToken && !keys) next({name: 'admin'})
                    if(!userToken && keys) next({name: 'admin-dashboard'}) 
                }
                next()                
            },

        },
        {
            path: '/subscribers',
            name: 'subscribers',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/home/Subscribers'),
        },
        {
            path: '/messenger/growth',
            name: 'growth',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/messenger/Growth'),
        },
        {
            path: '/messenger/abandoned-carts',
            name: 'messenger-abandoned-carts',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/messenger/AbandonedCards'),
        },
        {
            path: '/messenger/receipts',
            name: 'receipts',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/messenger/Receipts'),
        },
        {
            path: '/messenger/fulfillment',
            name: 'fulfillment',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/messenger/Fulfillment'),
        },
        {
            path: '/messenger/welcome',
            name: 'welcome',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/messenger/Welcome'),
        },
        {
            path: '/messenger/bot-menu',
            name: 'bot-menu',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/messenger/BotMenu'),
        },
        {
            path: '/email/abandoned-carts',
            name: 'email-abandoned-carts',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/email/abandoned carts/EmailAbandonedCards'),
        },
        {
            path: '/email/abandoned-carts/reminders/:id?',
            name: 'email-abandoned-carts-reminders',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/email/abandoned carts/AbandonedTableEdit'),
        },
        {
            path: '/email/campaigns',
            name: 'email-campaigns',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/email/campaigns/EmailCampaigns'),
        },
        {
            path: '/email/campaigns/edit/:id?',
            name: 'email-campaigns-add',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/email/campaigns/NewCampaign'),
        },
        {
            path: '/email/browse-abandonment/reminders/:id?',
            name: 'email-browse-abandonment',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/email/browse abandonment/BrowseTableEdit'),
        },
        {
            path: '/email/browse-abandonment',
            name: 'email-browse-abandonment-edit',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/email/browse abandonment/EmailBrowseAbandonment'),
        },
        {
            path: '/sms/abandoned-carts',
            name: 'sms-abandoned-carts',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/sms/abandoned carts/SmsAbandonedCarts'),
        },
        {
            path: '/sms/abandoned-carts/reminders/:id?',
            name: 'sms-abandoned-carts-reminders',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/sms/abandoned carts/SmsAbandonedTableEdit'),
        },
        {
            path: '/sms/countries',
            name: 'sms-countries',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/sms/countries/SmsCountries'),
        },
        {
            path: '/sms/campaigns',
            name: 'sms-campaigns',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/sms/campaigns/Index'),
        },
        {
            path: '/sms/campaigns/edit/:id',
            name: 'sms-campaigns-edit',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/sms/campaigns/Edit'),
        },
        {
            path: '/messenger/growth/widget',
            name: 'growth-widget',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./components/Messenger/growth/Widget'),
        },
        {
            path: '/growth-tools',
            name: 'growth-tools',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/growth-tools/Index'),
        },
        {
            path: '/growth-tools-edit/:id?',
            name: 'growth-tools-edit',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/growth-tools/Edit'),
        },
        {
            path: '/pricing',
            name: 'pricing',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/pricing/Pricing'),
        },
        // {
        //     path: '/push/growth',
        //     name: 'push-growth',
        //     meta: {layout: 'dashboard-layout'},
        //     component: () => import('./views/push/growth/index'),
        // },
        // {
        //     path: '/push/growth/pop-up',
        //     name: 'push-popup',
        //     meta: {layout: 'dashboard-layout'},
        //     component: () => import('./views/push/growth/EditSub'),
        // },
        {
            path: '/push/growth/welcome-push',
            name: 'welcome-popup',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/push/growth/EditWelcome'),
        },
        {
            path: '/push/abandoned-carts',
            name: 'push-abandoned-cards',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/push/abandoned-carts/Index'),
        },
        {
            path: '/push/abandoned-carts/edit/:id?',
            name: 'push-abandoned-cards-edit',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/push/abandoned-carts/Edit'),
        },
        {
            path: '/push/campaigns',
            name: 'push-campaigns',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/push/campaigns/Index'),
        },
        {
            path: '/push/campaigns/add/:id?',
            name: 'push-campaigns-add',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/push/campaigns/Edit'),
        },
        {
            path: '/login',
            name: 'login',
            meta: {layout: 'auth'},
            component: () => import('./views/login/Login'),
        },
        {
            path: '/admin/terminal',
            name: 'terminal',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/AdminTerminal'),
        },
        {
            path: '/admin',
            name: 'admin',
            meta: {layout: 'auth'},
            component: () => import('./views/super admin login/Login'),
            beforeEnter: (to, from, next) => {
                if(to.name == 'admin' && from.name !== 'dashboard') {
                    const keysAdminPanel = window.$cookies.get('token_wix') && window.$cookies.get('token_shopify')
                    const keyUserPanel = window.$cookies.get('userToken')
                    
                    if(keysAdminPanel && keyUserPanel) from.name !== 'dashboard' ? next({name: 'dashboard'}) : next()
                    else if(keysAdminPanel) from.name !== 'admin-dashboard' ? next({name: 'admin-dashboard'}) : next()
                    else next()
                } else next()
            },
        },
        {
            path: '/setup/1',
            name: 'setup-1',
            meta: {layout: 'setup-layout'},
            component: () => import('./components/setup/Setup_1'),
        },
        {
            path: '/setup/2',
            name: 'setup-2',
            meta: {layout: 'setup-layout'},
            component: () => import('./components/setup/Setup_4'),
        },
        {
            path: '/admin/dashboard',
            name: 'admin-dashboard',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/AdminDashboard'),
            beforeEnter: (to, from, next) => {
                sessionStorage.clear()
                const keys = window.$cookies.get('token_wix') && window.$cookies.get('token_shopify') 
                if(!keys) next({name: 'admin'})
                next()                
            },
        },
        {
            path: '/admin/shops',
            name: 'admin-shops',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/AdminShops'),
        },
        {
            path: '/admin/shops/:id?',
            name: 'admin-shops-edit',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/AdminShopsEdit'),
        },
        // {
        //     path: '/admin/shops/sales',
        //     name: 'admin-shops-sales',
        //     meta: {layout: 'super-admin-layout'},
        //     component: () => import('./views/super admin/AdminShopsSales'),
        // },
        {
            path: '/admin/users',
            name: 'admin-users',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/AdminUsers'),
        },
        {
            path: '/admin/:platform?/users/:id?',
            name: 'admin-user-edit',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/AdminUsersEdit'),
        },
        {
            path: '/admin/users/create',
            name: 'admin-user-create',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/AdminUsersCreate'),
        },
        {
            path: '/admin/affiliate',
            name: 'admin-affiliate',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/Affiliates'),
        },
        {
            path: '/admin/token-expiration-emails',
            name: 'admin-token-expiration-emails',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/TokenExpirationEmails'),
        },
        {
            path: '/admin/geo-permissions',
            name: 'admin-geo-permissions',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/AdminGeoPermissions'),
        },
        {
            path: '/admin/logs',
            name: 'admin-logs',
            meta: {layout: 'super-admin-layout'},
            component: () => import('./views/super admin/Logs'),
        },
        {
            path: '/settings',
            name: 'setting',
            meta: {layout: 'dashboard-layout'},
            component: () => import('./views/settings'),

        },
        {
            path: '/admin/discount-links',
            name: 'discount-links',
            meta: {layout: 'super-admin-layout', permissions: ['shopify']},
            component: () => import('./views/discount-links'),
        },
        {
            path: '/admin/discount-links/add',
            name: 'discount-add',
            meta: {layout: 'super-admin-layout', permissions: ['shopify']},
            component: () => import('./views/discount-links/Add.vue'),
        },
        {
            path: '/admin/discount-links/edit',
            name: 'discount-edit',
            meta: {layout: 'super-admin-layout', permissions: ['shopify']},
            component: () => import('./views/discount-links/Edit.vue'),
        },
        {
            path: '*',
            redirect: '/dashboard',
        }
        
    ],
    mode: 'history'
})


router.beforeEach( async(to, from, next) => {
    const permissions = to.meta.permissions
    const platform  = window.$cookies.get('platform')

    const platformQuery = to.query.platform
    const token = to.query.token
    if(token || platformQuery) {
        window.$cookies.set('userToken', token)
        window.$cookies.set('platform', platformQuery)
    }
    
    if (permissions) {
        if(!permissions.includes(platform)) {
            next({name: 'dashboard'})
        } else next()
    } else  next()
})

export default router
