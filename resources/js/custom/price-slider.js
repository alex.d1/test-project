import noUiSlider from "nouislider";
import 'nouislider/distribute/nouislider.css';

$(document).ready(function () {
    slider('#slider-target');
});


export function slider(targetSelector) {
    var target = $(targetSelector);

    if (!target.length) {
        return;
    }
    var formatPrice = function (value) {
        return (
            '$' +
            parseFloat(value)
                .toFixed(2)
                .replace(/\.00/, '')
                .replace(/\B(?=(\d{3})+(?!\d))/g, ',')
        );
    };

    var pricingLevels = [
        {
            from: 0,
            to: 100,
            fee: 0
        },
        {
            from: 101,
            to: 200,
            fee: 4.99
        },
        {
            from: 201,
            to: 500,
            fee: 9.99
        },
        {
            from: 501,
            to: 1000,
            fee: 19.99
        },
        {
            from: 1001,
            to: 2000,
            fee: 39.99
        },
        {
            from: 2001,
            to: 5000,
            fee: 89.99
        },
        {
            from: 5001,
            to: 10000,
            fee: 169.99
        },
        {
            from: 10001,
            to: 20000,
            fee: 249.99
        },
        {
            from: 20001,
            to: 50000,
            fee: 499.99
        },
        {
            from: 50001,
            to: 100000,
            fee: 999.99
        },
        {
            from: 50001,
            to: 100000,
            fee: '$999.99+'
        }
    ];

    // pricingLevels.push({
    //   from: '',
    //   to: pricingLevels[pricingLevels.length - 1].to,
    //   fee: 'Contact us'
    // });

    initSlider(pricingLevels);

    function initSlider(data) {
        var priceVal = $('#price-val');
        var extraSalesVal = $('.pricing-slider__extra-sales-value');
        var noUiConnect = $('.noUi-connect');

        var config = {
            start: [1],
            connect: [true, false],
            range: {
                min: 1,
                max: data.length
            },
            pips: {
                mode: 'steps',
                filter: function (value) {
                    return value % 1 ? 0 : 1;
                }
            },
            step: 1,
            tooltips: true
        };

        var slider = noUiSlider.create(target[0], config);
        slider.on('update', function (values, handle, unencoded) {
            var indx = Math.round(+unencoded) - 1;
            var isLastIndex = data.length - 1 === indx;
            var fee = formatPrice(data[indx].fee);
            var to = formatPrice(data[indx].to);

            if (indx >= 1) {
                $('.sms-costs-label').css('display', 'block')
                //   $('.pricing-slider__slider-box').css('margin', '67px 0px 0px 0px')
            } else {
                $('.sms-costs-label').css('display', 'none')
                // $('.pricing-slider__slider-box').css('margin', '91px 0px 0px 0px')
            }

            let element = $('.pricing-slider__price-box__price-label, .pricing-slider__up-to');
            let one_more_element = $('.pricing-slider__price-box__price-val');
            let toggle_class_name = 'pricing-slider__price-box__price-val--last';

            if (isLastIndex) {
                fee = data[indx].fee;
                to += '+';

                element.css('visibility', 'hidden');
                one_more_element.addClass(toggle_class_name);
            }
            else {
                element.css('visibility', 'visible');
                one_more_element.removeClass(toggle_class_name);
            }

            priceVal.html(fee);
            extraSalesVal.html(to);
        });
    }
}
