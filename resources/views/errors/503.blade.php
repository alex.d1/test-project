<!doctype html>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/all.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.1/css/v4-shims.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<title>Site Maintenance</title>
<style>
    body {
        font-family:  "Roboto", "Helvetica Neue",'Helvetica','Arial','sans-serif'; !important;
    }
    .error {
        position: relative;
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        text-align: center;
        min-height: 100vh;
        width: 100%;
        padding: 32px 16px;
        box-sizing: border-box;
    }

    .error__logo {
        position: absolute;
        left: 48px;
        top: 33px;
    }
    .error__title {
        font-weight: bold;
        font-size: 80px;
        line-height: 94px;
        color: #1B2437;
        margin-top: 30px;
        margin-bottom: 24px;
    }
    .error__text {
        display: block;
        font-size: 18px;
        line-height: 150%;
        color: #282828;
        margin-bottom: 6px;
    }
    .error__btn {
        margin-top: 24px;
        font-weight: 500;
        font-size: 14px;
        line-height: 16px;
        background: #4786FF;
        width: 192px;
        height: 48px;
        color: #fff;
        border: none;
        cursor: pointer;
    }
    @media(max-width: 770px) {
        .error {
            padding-top: 64px;
        }
        .error__title {
            font-size: 60px;
            line-height: 64px;
        }
        .error__logo {
            left: 16px;
            top: 16px;
        }
    }

</style>

<div class="error">
    <a class="error__logo" target="_blank" href="https://carti.io/"><img src="/images/logo-503.svg" alt="logo"></a>
    
    <img
        src="/images/building.svg"
        alt="build"
    >
    <h1 class="error__title">
        We'll be right back!
    </h1>
    <span class="error__text">Sorry, we are down for the maintenance, but we'll be back in no time.</span>
    <span class="error__text">If you have any question, feel free to contact us.</span>
    <button class="error__btn" onclick="$crisp.push(['do', 'chat:open'])">Contact Us</button>


</div>

<!-- Crisp -->
<script type="text/javascript">window.$crisp = [];
    window.CRISP_WEBSITE_ID = "f4e390ff-ce10-47e9-aea1-c17fdf2a5d78";
    (function () {
        d = document;
        s = d.createElement("script");
        s.src = "https://client.crisp.chat/l.js";
        s.async = 1;
        d.getElementsByTagName("head")[0].appendChild(s);
    })();</script>
<!-- Crisp -->